---
layout: job_page
title: "Field Marketing Manager"
---

## Responsibilities

* Swag management.
* Creation of swag products.
* Selection and coordination with vendors.
* Decision making and discretion regarding event selection and planning. 
* Event strategy and decision making.
* Event logistics in support of the team. From helping to book space for meetups to making sure the booth is staffed, and making sure every aspect of our events are well organized.
